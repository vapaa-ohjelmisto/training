#!/bin/bash
#!/usr/bin/bash # for wsl

# before version update, check out the compatibility matrix from here: https://docs.asciidoctor.org/reveal.js-converter/latest/setup/compatibility-matrix/
#REVEAL_JS_VERSION=4.3.1
REVEAL_JS_VERSION=3.9.2

# Pre checks
bundler -v &> /dev/null || { printf "\nBundler not installed!\n\n"; exit 1; }
git --version &> /dev/null || { printf "\nGit not installed!\n\n"; exit 1; }


# Install ruby stuff
bundle config --local path .bundle/gems
bundle

[ -d "./slides/reveal.js" ] || git clone -b "$REVEAL_JS_VERSION" --depth 1 https://github.com/hakimel/reveal.js.git ./slides/reveal.js && echo "Directory reveal.js exists, skipping git pull"

