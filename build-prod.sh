#!/bin/bash

# Running this from linux-training root will generate new 
# content into ..pages/training/ which is outside of this repo
# before using, clone pages alongside linux-training

# before version update, check out the compatibility matrix from here: https://docs.asciidoctor.org/reveal.js-converter/latest/setup/compatibility-matrix/
bundle exec asciidoctor-revealjs -a revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@3.9.2 "./linux/index.adoc"
bundle exec asciidoctor-revealjs -a revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@3.9.2 "./ansible/index.adoc"


cp ./linux/index.html ../pages/training/linux/index.html
cp ./ansible/index.html ../pages/training/ansible/index.html

cp -r ./linux/media ../pages/training/linux/media