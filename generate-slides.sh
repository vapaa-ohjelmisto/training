#!/bin/bash
#!/usr/bin/bash # for wsl

[ -n "$1" ] || { echo "Requires filename as argument"; exit 1; }

bundle exec asciidoctor-revealjs "$1"